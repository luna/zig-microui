const Builder = @import("std").build.Builder;

// TODO: port this bit from the demo build script:
//
// OS_NAME=`uname -o 2>/dev/null || uname -s`
//
// if [ $OS_NAME == "Msys" ]; then
//     GLFLAG="-lopengl32"
// elif [ $OS_NAME == "Darwin" ]; then
//     GLFLAG="-framework OpenGL"
// else
//     GLFLAG="-lGL"
// fi
//

const c_args = [_][]const u8{
    "-Wall",
    "-std=c11",
    "-pedantic",

    // prevent sigill
    "-fno-sanitize=undefined",
};

const source_files = [_][]const u8{
    "src/main.c", "src/renderer.c", "microui-src/microui.c",
};

pub fn build(b: *Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const exe = b.addExecutable("microui-demo", "src/main.zig");
    exe.linkLibC();
    exe.addIncludeDir("./src");
    exe.addIncludeDir("./microui-src");
    for (source_files) |source| {
        exe.addCSourceFile(source, &c_args);
    }

    exe.linkSystemLibrary("SDL2");
    exe.linkSystemLibrary("GL");

    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
