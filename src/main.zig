const std = @import("std");

const c = @cImport({
    @cInclude("SDL2/SDL.h");
    @cInclude("microui.h");
    @cInclude("renderer.h");
});

export fn text_width(font: c.mu_Font, text: [*c]const u8, len: c_int) c_int {
    var actual_len: c_int = len;
    if (actual_len == -1) {
        actual_len = @intCast(c_int, std.mem.len(text));
    }

    return c.r_get_text_width(text, actual_len);
}

export fn text_height(font: c.mu_Font) c_int {
    // TODO handle font data?
    return c.r_get_text_height();
}

fn toMuMouse(sdl_mouse: u8) u8 {
    return switch (sdl_mouse) {
        c.SDL_BUTTON_LEFT => c.MU_MOUSE_LEFT,
        c.SDL_BUTTON_RIGHT => c.MU_MOUSE_RIGHT,
        c.SDL_BUTTON_MIDDLE => c.MU_MOUSE_MIDDLE,
        else => 0,
    };
}

fn toMuKey(sdl_key: i32) u8 {
    return switch (sdl_key) {
        c.SDLK_LSHIFT => c.MU_KEY_SHIFT,
        c.SDLK_RSHIFT => c.MU_KEY_SHIFT,
        c.SDLK_LCTRL => c.MU_KEY_CTRL,
        c.SDLK_RCTRL => c.MU_KEY_CTRL,
        c.SDLK_LALT => c.MU_KEY_ALT,
        c.SDLK_RALT => c.MU_KEY_ALT,
        c.SDLK_RETURN => c.MU_KEY_RETURN,
        c.SDLK_BACKSPACE => c.MU_KEY_BACKSPACE,

        else => 0,
    };
}

fn testWindow(ctx: *c.mu_Context) void {
    if (c.mu_begin_window(ctx, "test", c.mu_rect(350, 40, 300, 200)) != 0) {
        c.mu_end_window(ctx);
    }
}

fn processFrame(ctx: *c.mu_Context) void {
    c.mu_begin(ctx);
    testWindow(ctx);
    c.mu_end(ctx);
}

pub fn main() anyerror!void {
    _ = c.SDL_Init(c.SDL_INIT_EVERYTHING);
    c.r_init();

    var ctx = try std.heap.c_allocator.create(c.mu_Context);
    defer std.heap.c_allocator.destroy(ctx);

    c.mu_init(ctx);

    ctx.text_width = text_width;
    ctx.text_height = text_height;

    while (true) {
        var e: c.SDL_Event = undefined;
        while (c.SDL_PollEvent(&e) != 0) {
            switch (e.@"type") {
                c.SDL_QUIT => std.os.exit(0),
                c.SDL_MOUSEMOTION => c.mu_input_mousemove(ctx, e.motion.x, e.motion.y),
                c.SDL_MOUSEWHEEL => c.mu_input_scroll(ctx, 0, e.motion.y * -30),
                c.SDL_TEXTINPUT => c.mu_input_text(ctx, &e.text.text),

                c.SDL_MOUSEBUTTONDOWN, c.SDL_MOUSEBUTTONUP => {
                    const b = toMuMouse(e.button.button);
                    if (b != 0 and e.type == c.SDL_MOUSEBUTTONDOWN) {
                        c.mu_input_mousedown(ctx, e.button.x, e.button.y, b);
                    }
                    if (b != 0 and e.type == c.SDL_MOUSEBUTTONUP) {
                        c.mu_input_mouseup(ctx, e.button.x, e.button.y, b);
                    }
                },

                c.SDL_KEYDOWN, c.SDL_KEYUP => {
                    var mu_char = toMuKey(e.key.keysym.sym);

                    if (mu_char != 0 and e.@"type" == c.SDL_KEYDOWN) {
                        c.mu_input_keydown(ctx, mu_char);
                    }
                    if (mu_char != 0 and e.@"type" == c.SDL_KEYUP) {
                        c.mu_input_keyup(ctx, mu_char);
                    }
                },
                else => {},
            }
        }

        processFrame(ctx);

        var clr = c.mu_Color{ .r = 255, .g = 0, .b = 0, .a = 255 };

        std.debug.warn("color: {}\n", .{clr});

        c.r_clear(clr);
        std.debug.warn("sex\n", .{});
        var cmd: ?*c.mu_Command = null;
        while (c.mu_next_command(ctx, &cmd) != 0) {
            switch (cmd.?.@"type") {
                c.MU_COMMAND_TEXT => c.r_draw_text(&cmd.?.text.str, cmd.?.text.pos, cmd.?.text.color),
                c.MU_COMMAND_RECT => c.r_draw_rect(cmd.?.rect.rect, cmd.?.rect.color),
                c.MU_COMMAND_ICON => c.r_draw_icon(cmd.?.icon.id, cmd.?.icon.rect, cmd.?.icon.color),
                c.MU_COMMAND_CLIP => c.r_set_clip_rect(cmd.?.clip.rect),
                else => {},
            }
        }

        c.r_present();
    }
}
